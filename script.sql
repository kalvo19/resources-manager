CREATE DATABASE slim_basics;

USE resources-manager;

CREATE TABLE users (
	id int(11) NOT NULL AUTO_INCREMENT,
    name varchar(255),
    email varchar(255),
    password varchar(255),
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    PRIMARY KEY (id)
);

INSERT INTO users (name, email) VALUES ('Stifmeister', 'stifmeister@hotmail.com');