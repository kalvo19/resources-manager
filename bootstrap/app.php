<?php

use Respect\Validation\Validator as v;

/* Carga todas las dependencias de la aplicación que están situadas en el directorio
 * 'vendor' que crea Composer cuando creamos el proyecto en slim con el comando => composer require slim/slim
 */
require_once __DIR__ . '/../vendor/autoload.php';

/*
 * Se inicia la sesión para poder mostrar por ejemplo, mensajes de error en algún formulario,
 * comprobar si el usuario ya se había registrado en la página web
 */
session_start();

/*
 * Al hacer el autoload mediante PSR-4 en el 'package.json' no es necesario cargar los modelos de la aplicación
 * manualmente en este fichero. De esta forma se ahorra escribir el 'namespace' que apunte a cualquier modelo, por
 * ejemplo al de usuarios.
 *
 * $user = new \App\Models\User
 */

$app = new \Slim\App([
   'settings' => [
       'displayErrorDetails' => true,
       'db' => [ /* Configuración de la base de datos */
           'driver' => 'mysql', // Tipo de servidor de base de datos que se va a utilizar
           'host' => 'localhost', // Nombre del servidor de base de datos
           'database' => 'slim_basics', // Nombre de la base de datos que se va a conectar
           'username' => 'root', // Usuario de la base de datos
           'password' => '', // Contraseña del usuario
           'charset' => 'utf8', // Codificación de carácteres
           'collation' => 'utf8_unicode_ci',
           'prefix' => '' // Prefijo
       ]
   ],
]);

/*
 * 'container' és una clase que esta dentro de la Classe App de Slim y básicamente es un contenedor
 *  de servicios. Como servicios podemos incluir la entrega de mensajes de correo electrónico, o la conexión
 *  a una base de datos. A diferencia de los modelos, los servicios són clases que están
 *  a nivel global en la aplicación y van a ser llamados desde diferentes puntos de la aplicación por lo que
 *  al tenerlos globalmente no será necesario instanciarlos cada vez que se vayan a utilizar.
 *
 * Ver más: https://gitnacho.github.io/symfony-docs-es/book/service_container.html
 */
$container = $app->getContainer();

/* Obtiene un objeto Manager que permitirá hacer una gestión de los modelos y de las tablas del servidor SQL que estan
 * asociadas a estos modelos.
 */
$capsule = new \Illuminate\Database\Capsule\Manager;

/* El método 'addConnection()' de la clase 'Manager' crea una conexión con la base de datos con los
 * parámetros creados en '$app['settings']['db']', que están a su vez en el contendedor de
 * servicios de slim ($container).
 *
 * Primer parámetro: vector associativo con los parámetros de la base de datos
 */
$capsule->addConnection($container['settings']['db']);

/*
* Hace la isntancia a '\Illuminat\Database\Capsule\Manager' global para que podamos acceder a sus métodos
* en cualquier momento. TODO
*/
$capsule->setAsGlobal();

/*
 * Arranca Eloquent
 */
$capsule->bootEloquent();

/*
 * Añade Eloquent al contenedor de servicios de Slim. Eloquent és un ORM (Object Relational Mapping), una técnica
 * de programación para convertir datos entre el sistema de tipos (string, number, Array) y la utilización de una base
 * de datos relacional como MySQL. De esta manera el desarrollador puede hacer consultas a la base de datos si necesidad
 * de realizar las consultas en lenguaje SQL.
 *
 * Para instalar Eloquent se ha de instalar el paquete 'Illuminate/database' a través de composer => composer install Illuminate/database
 *
 * Ver más: https://laravel.com/docs/5.8/eloquent
 */
$container['db'] = function($container) use ($capsule) {
    return $capsule;
};

$container['AuthController'] = function($container) {
    return new NombreDeLaAplicacion\Controllers\Auth\AuthController($container);
};

/*
 * Añade al contenedor de servicios de Slim el motor de plantillas 'Twig'
 *
 * Ver maś: https://twig.symfony.com/
 */
$container['view'] = function($container) {

    /*
     * Llama al constructor del motor de plantillas 'Twig' y le especifica donde van estar alamacenadas todas las
     * vistas. Y le indica algunos parámetros de configuración, en este caso esta señala que las vistas van a ser
     * compiladas cada vez que ser cargue el servidor, por lo que no serán cacheadas.
     */
    $view = new \Slim\Views\Twig(__DIR__ . '/../resources/views', [
        'cache' => false,
    ]);

    /*
     * Añade una variable global llamada 'auth' que permite tener el usuario que ha iniciado
     * la sesión disponible en todas las vistas de la aplicación.
     */
    $view->getEnvironment()->addGlobal('auth', [
        'user' => $container->auth->user(),
        'check' => $container->auth->check()
    ]);

    /*
     * Añade una variable llamada 'flash' que contiene todos los mensajes que se van a mostrar
     * en las diferentes vistas de la aplicación, por ese mismo motivo al igual que 'auth' se
     * añade globalmente. '$container->flash' equivale a la clase \Slim\Flash\Messages que tiene
     * a su disposición métodos que permiten recoger los mensajes en la vista (getMessage()).
     */
    $view->getEnvironment()->addGlobal('flash', $container->flash);

    /*
     * TODO
     */
    $view->addExtension(new \Slim\Views\TwigExtension(
        $container->router,
        $container->request->getUri()
    ));

    return $view;
};

$container['validator'] = function($container) {
    return new \NombreDeLaAplicacion\Validation\Validate;
};

/*
 * TODO
 */
$container['HomeController'] = function($container) {
    return new NombreDeLaAplicacion\Controllers\HomeController($container);
};

/*
 * Añade Cross Site Request Forgery al contenedor de servicios de Slim para que pueda ser utilizado
 * en los formularios de la aplicación y evitar así la entrada de datos de usuarios que no esten visualizando
 * la web como forma de ataque.
 */
$container['csrf'] = function($container) {
    return new \Slim\Csrf\Guard;
};

$container['auth'] = function($container) {
    return new NombreDeLaAplicacion\Auth\Auth;
};

$container['flash'] = function($container) {
    return new \Slim\Flash\Messages();
};

/*
 * Permite usar el Middleware 'ValidationErrorsMiddleware' en nuestra aplicación.
 */
$app->add(new NombreDeLaAplicacion\Middleware\ValidationErrorsMiddleware($container));

$app->add(new NombreDeLaAplicacion\Middleware\OldInputMiddleware($container));

$app->add(new NombreDeLaAplicacion\Middleware\csrfViewMiddleware($container));

/*
 * Activa la classe Cross Site Request Forgery para que cada vez que se carga una página se disponga
 * de un token generado aleatoriamente para esa petición. De manera que cada usuario que haga una petición,
 * este tenga uno distinto al resto.
 */
$app->add($container['csrf']);

v::with('NombreDeLaAplicacion\\Validation\\Rules');

require_once __DIR__ . '/../app/routes.php';

