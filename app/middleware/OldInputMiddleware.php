<?php
/**
 * Created by PhpStorm.
 * User: kalvo19
 * Date: 16/05/2019
 * Time: 23:58
 */

namespace NombreDeLaAplicacion\Middleware;


class OldInputMiddleware extends Middleware
{
    public function __invoke($request, $response, $next)
    {
        if (isset($_SESSION['old'])) {
            $this->container->view->getEnvironment()->addGlobal('old', $_SESSION['old']);
        }
        $_SESSION['old'] = $request->getParams();

        $response = $next($request, $response);
        return $response;
    }
}