<?php
/**
 * Created by PhpStorm.
 * User: kalvo19
 * Date: 18/05/2019
 * Time: 15:53
 */

namespace NombreDeLaAplicacion\Middleware;


class csrfViewMiddleware extends Middleware
{
    public function __invoke($request, $response, $next)
    {
        $this->container->view->getEnvironment()->addGlobal('csrf', [
            'fields' => '
                        <input type="hidden" name="' . $this->container->csrf->getTokenNameKey() . '" value="' . $this->container->csrf->getTokenName() .'"> 
                        <input type="hidden" name="' . $this->container->csrf->getTokenValueKey() . '" value="' . $this->container->csrf->getTokenValue() . '">
        ']);

        $response = $next($request, $response);
        return $response;
    }
}