<?php

/*
 * El método 'setName()' sirve para darle un nombre a esa ruta en particular, de esta manera la ruta
 * podrá ser accesible utilizando el método 'pathFor()' o el helper 'path_for()' y el nombre pasado como parámetro.
 *
 * Véase el fichero 'AuthController.php' que contiene una redirección a esta misma ruta para poder entender
 * mejor esta explicación.
 */
$app->get('/', 'HomeController:index')->setName('home');

$app->get('/auth/signup', 'AuthController:showSignUp')->setName('auth.signup');
$app->post('/auth/signup', 'AuthController:sendSignUp');

$app->get('/auth/signin', 'AuthController:showSignIn')->setName('auth.signin');
$app->post('/auth/signin', 'AuthController:sendSignIn');

$app->get('/auth/signout', 'AuthController:sendSignOut')->setName('auth.signout');