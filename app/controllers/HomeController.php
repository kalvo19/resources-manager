<?php
/**
 * User: kalvo19
 * Date: 17/04/2019
 * Time: 23:41
 */

/* NOTA: El 'namespace' que apunta al directorio '/app' tiene que coincidir con el nombre que
 * se introdujo en el objeto autoload del 'package.json'.
 */
namespace NombreDeLaAplicacion\Controllers;

use NombreDeLaAplicacion\Models\User as User;
use Slim\Views\Twig as View;

class HomeController extends Controller
{
    public function index($request, $response) {
        return $this->view->render($response, 'home.twig');
    }
}