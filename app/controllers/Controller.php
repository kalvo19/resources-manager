<?php
/**
 * User: kalvo19
 * Date: 19/04/2019
 * Time: 12:56
 */

namespace NombreDeLaAplicacion\Controllers;



class Controller
{
    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    /*
     *
     */
    public function __get($property)
    {
        if ($this->container->{$property}) {
            return $this->container->{$property};
        }
    }
}