<?php
/**
 * Created by PhpStorm.
 * User: kalvo19
 * Date: 22/04/2019
 * Time: 18:49
 */

namespace NombreDeLaAplicacion\Controllers\Auth;

use NombreDeLaAplicacion\Controllers\Controller;
use NombreDeLaAplicacion\Models\User;
use Respect\Validation\Validator as v;
use Slim\Views\Twig as View;

class AuthController extends Controller
{
    /*
     * Carga el formulario de Login de la aplicación
     */
    public function showSignIn($request, $response) {
        $this->view->render($response, 'auth/signin.twig');
    }

/*
 * Gestiona la información recibida en el formulario 'signin.twig'
 */
    public function sendSignIn($request, $response){

        /*
         * Comprueba que el correo y la contraseña recogidos en 'auth.signin' sean correctos.
         */
        $auth = $this->auth->attempt(
            $request->getParam('email'),
            $request->getParam('password')
        );


        // Si no són correctos se redirecciona el usuario al formulario de login 'auth.signin'
        if (!$auth) {
            return $response->withRedirect($this->router->pathFor('auth.signin'));
        }

        $this->flash->addMessage('success', 'You has been logged');

        // Si són correctos se redirecciona el usuario a su página de inicio 'home'
        return $response->withRedirect($this->router->pathFor('home'));
    }

    /*
     * Carga el formulario de registro de la aplicación
     */
    public function showSignUp($request, $response) {
        $this->view->render($response, 'auth/signup.twig');
    }

    /*
     * Envia la información de registro del usuario provinente del formulario del 'signup.twig' para que sea
     * registrada en la base de datos.
     */
    public function sendSignUp($request, $response) {
        /*
         * Configura las reglas que tendrán que cumplir todos los campos del formulario de 'signup.twig'
         */
        $validation = $this->validator->validate($request, [
            'name' => v::alpha()->notEmpty(), // Carácter alfabético / No puede estar vacío / Sin espacios en blanco
            'email' => v::email()->noWhitespace()->notEmpty()->emailAvailable(), // Formato de correo / No puede estar vacío
            'password' => v::notEmpty()->length(8,15), // No puede estar vacío / Debe de tener al menos 8 carácteres y 15 como máximo
        ]);

        /*
         * Comprueba si el vector que recoge todos los errores y excepciones del formulario de 'signup.twig'
         * no está vacío. En ese caso redirige al usuario de nuevo al Sign Up para que rellene de nuevo el formulario.
         */
        if ($validation->failed()) {
            return $response->withRedirect($this->router->pathFor('auth.signup'));
        }

        $user = User::create([
            'name' => $request->getParam('name'),
            'email' => $request->getParam('email'),
            'password' => password_hash($request->getParam('password'), PASSWORD_DEFAULT),  // Encripta la contraseña
        ]);

        $this->auth->attempt($user->email, $request->getParam('password'));

        /*
         * Esto redirige al usuario a la página de 'home.twig'
         */
        return $response->withRedirect($this->router->pathFor('home'));
        // NOTA: 'home' equivale al nombre que se le ha dado en 'routes.php' con el método 'setNames()'
    }

    public function sendSignOut($request, $response) {
        $this->auth->logout();

        return $response->withRedirect($this->router->pathFor('home'));
    }
}