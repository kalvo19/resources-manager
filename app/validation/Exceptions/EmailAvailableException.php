<?php
/**
 * Created by PhpStorm.
 * User: kalvo19
 * Date: 17/05/2019
 * Time: 0:37
 */

namespace NombreDeLaAplicacion\Validation\Exceptions;

use Respect\Validation\Exceptions\ValidationException;


class EmailAvailableException extends ValidationException
{
    public static $defaultTemplates = [
        self::MODE_DEFAULT => [
            self::STANDARD => 'Email is already taken.',
        ]
    ];
}