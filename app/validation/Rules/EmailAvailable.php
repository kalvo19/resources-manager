<?php
/**
 * Created by PhpStorm.
 * User: kalvo19
 * Date: 17/05/2019
 * Time: 0:32
 */

namespace NombreDeLaAplicacion\Validation\Rules;

use Respect\Validation\Rules\AbstractRule;
use NombreDeLaAplicacion\Models\User;

class EmailAvailable extends AbstractRule
{
    public function validate($input)
    {
        return User::where('email', $input)->count() === 0;
    }
}