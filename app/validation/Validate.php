<?php
/**
 * Created by PhpStorm.
 * User: kalvo19
 * Date: 22/04/2019
 * Time: 23:27
 */

namespace NombreDeLaAplicacion\Validation;

use Respect\Validation;
use Respect\Validation\Exceptions\NestedValidationException;

class Validate
{
    protected $errors;

    /*
     * Comprueba a través de Respect\Validaton que todos los campos que se han introducido en el request del
     * formulario cumplen con las lista de reglas pasadas como segundo parámetro.
     *
     * @param $request
     * @param array $rules
     */
    public function validate($request, $rules = []) {
        foreach ($rules as $field => $rule) {
            try {

                /*
                 * Cambia el nombre de la regla con el nombre del campo con la primera letra en mayúscula
                 * para que en caso de error el mensaje a enviar al usuario sea más amigable.
                 */
                $rule->setName(ucfirst($field))->assert($request->getParam($field));
            } catch (NestedValidationException $exception) {

                /* En caso de que el campo no cumpla con la regla associada a ella, guarda el mensaje en
                 * en la propiedad '$errors' de la clase, para que sea mostrada en la vista.
                 */
                $this->errors[$field] = $exception->getMessages();
            }
        }

        $_SESSION['errors'] = $this->errors;

        return $this;   
    }

    /*
     * Retorna verdadero si la propiedad '$errors' contiene mensajes de error. Falso en caso que se de lo contrario.
     */
    public function failed() {
        return !empty($this->errors);
    }
}