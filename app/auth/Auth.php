<?php
/**
 * Created by PhpStorm.
 * User: kalvo19
 * Date: 18/05/2019
 * Time: 17:39
 */

namespace NombreDeLaAplicacion\Auth;

use NombreDeLaAplicacion\Models\User;

class Auth
{

    /*
     * Retorna el usuario de la actual sesión.
     */
    public function user() {
        if (isset($_SESSION['user'])) {
            return User::find($_SESSION['user']);
        }

        return null;
    }

    /*
     * Comprueba si ya hay un usario configurado en la sesión.
     */
    public function check() {
        return isset($_SESSION['user']);
    }

    /*
     * Comprueba si el usuario entrado en el formulario de login existe ya en la base de datos
     * y si es así setea en la sesión la ID del usuario que se ha identificado.
     */
    public function attempt($email, $password) {

        /*
         * Selecciona el usuario que coincide con el correo entrado en el formulario de 'auth.signin'
         * en la base de datos.
         */
        $user = User::where('email', $email)->first();

        /*
         * Si la consulta no tiene coincidencias quiere decir que no hay un usuario con ese correo
         * y por lo tanto no es necesario comprobar la contraseña, por lo que acaba retornando false.
         */
        if (!$user) {
            return false;
        }

        /*
         * Una vez se ha comprobado que el correo existe, se revisa que la contraseña
         * que se ha escrito coincide con la que tiene encriptada el usuario asociado
         * en la base de datos. En ese caso retorna true.
         * NOTA: El método 'password_verify()' comprueba que la cadena pasada como primer parámetro
         * coincide con la cadena encriptada en MD5.
         */
        if (password_verify($password, $user->password)) {

            // Añade la ID del usuario que se ha logueado a la sesión en marcha y retorna true.
            $_SESSION['user'] = $user->id;
            return true;
        }

        return false;
    }

    public function logout() {
        unset($_SESSION['user']);
    }
}