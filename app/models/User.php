<?php
/**
 * User: kalvo19
 * Date: 22/04/2019
 * Time: 17:31
 */

namespace NombreDeLaAplicacion\Models;

use Illuminate\Database\Eloquent\Model;

/*
 * Por defecto Eloquent hace la transcripción de la clase 'User' que esta en singular a la palabra 'users'
 * escrita en plural que se corresponde con el nombre de la tabla SQL
 */
class User extends Model
{
    protected $table = 'users';

    protected $fillable = [
        'name',
        'email',
        'password'
    ];
}